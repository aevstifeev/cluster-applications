FROM "registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/2.16.1-kube-1.13.12"

COPY src/ build/

RUN wget https://github.com/roboll/helmfile/releases/download/v0.94.1/helmfile_linux_amd64 \
  && mv helmfile_linux_amd64 /usr/local/bin/helmfile \
  && chmod u+x /usr/local/bin/helmfile

RUN apk add --no-cache bash

RUN helm init --client-only \
  && helm plugin install https://github.com/databus23/helm-diff \
  && helm plugin install https://github.com/aslafy-z/helm-git.git --version 0.5.0

RUN ln -s /build/bin/* /usr/local/bin/ \
  && ln -s /build/default-data /usr/local/share/gitlab-managed-apps

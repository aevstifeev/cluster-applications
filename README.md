# Cluster Applications (alpha)

End goal: Execute all helm operations as GitLab CI yml in the connected
cluster repo (vs running on server).

## Application

To install an application, TBC

## Development

Scripts in this repository follow GitLab's
[shell scripting guide](https://docs.gitlab.com/ee/development/shell_scripting_guide/)
and enforces `shellcheck` and `shfmt`.

## Testing

This project's CI uses [`k3s`](https://k3s.io/) clusters to test installing
applications. We also use test installation of applications on a
dedicated GKE cluster on the `master` branch.

Due to an issue with [installing Sentry on
k3s](https://gitlab.com/gitlab-org/cluster-integration/cluster-applications/issues/7),
we only test Sentry installation on the GKE cluster. If you wish to run
installation tests for GKE on your branch, create a new pipeline with
the `TEST_GKE_CLUSTER` variable set to any value.

## Contributing and Code of Conduct

Please see [CONTRIBUTING.md](CONTRIBUTING.md)
